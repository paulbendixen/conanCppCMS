from conans import ConanFile, CMake, tools


class CppcmsConan(ConanFile):
    name = "cppcms"
    version = "1.2.1"
    license = "BSD"
    author = "Paul M. Bendixen <paulbendixen@gmail.com>"
    url = "https://gitlab.com/paulbendixen/conanCppCMS"
    description = "CppCMS is a web development framework for performance demanding applications."
    topics = ("Web", "html")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"

    def _configure_cmake(self):
        cmake = CMake(self)
        if ( self.options.shared ):
            configdefs = {"CMAKE_INSTALL_RPATH":"$ORIGIN"}
            configdefs["DISABLE_STATIC"]="ON"
        else:
            configdefs= {"DISABLE_SHARED":"ON"}
        cmake.configure( defs=configdefs, source_folder="cppcms-{}".format( self.version ) )
        return cmake

    def source(self):
        tools.get( "https://sourceforge.net/projects/cppcms/files/cppcms/1.2.1/cppcms-{}.tar.bz2/download".format( self.version ), destination=".", filename="cppcms.tar.bz2" )

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

